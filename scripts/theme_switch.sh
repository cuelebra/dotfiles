#!/bin/bash

THEME=$(cat ~/.NIGHTDAY)
if [ "$THEME" = "night" ]; then
	cp ~/Documents/scripts/extra/settings.ini.day ~/.config/gtk-3.0/settings.ini
	cp ~/Documents/scripts/extra/.gtkrc-2.0.day ~/.gtkrc-2.0
	kvantummanager --set Adapta
	echo "day" > ~/.NIGHTDAY
	echo "Theme set to Light"
else
	cp ~/Documents/scripts/extra/settings.ini.night ~/.config/gtk-3.0/settings.ini
	cp ~/Documents/scripts/extra/.gtkrc-2.0.night ~/.gtkrc-2.0
	kvantummanager --set AdaptaNokto
	echo "night" > ~/.NIGHTDAY
	echo "Theme set to DarK"
fi


