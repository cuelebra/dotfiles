#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Sort of mini driver.
Read a specific InputDevice (my_mx_mouse),
monitoring for special thumb button
Use uinput (virtual driver) to create a mini keyboard
Send ctrl keystroke on that keyboard
"""

from evdev import InputDevice, categorize, ecodes
import uinput

# Initialize keyboard, choosing used keys
ctrl_mouse = uinput.Device([
    uinput.BTN_LEFT,
    uinput.BTN_RIGHT,
    uinput.REL_X,
    uinput.REL_Y,
    ])

# Sort of initialization click (not sure if mandatory)
# ( "I'm-a-keyboard key" )
#ctrl_mouse.emit_click(uinput.KEY_KEYBOARD)

# Useful to list input devices
#for i in range(0,15):
#    dev = InputDevice('/dev/input/event{}'.format(i))
#    print(dev)

# Declare device patch.
# I made a udev rule to assure it's always the same name
dev = InputDevice('/dev/my_qf_keyboard')
#print(dev)
ctrlkey_on = False

# Infinite monitoring loop
for event in dev.read_loop():
    # My thumb button code (use "print(event)" to find)
    #print(event) 58 for caps, 86 for <
    if event.code == 58:
        # Button status, 1 is down, 0 is up
        if event.value == 1:
            ctrl_mouse.emit(uinput.BTN_LEFT, 1)
            ctrlkey_on = True
        elif event.value == 0:
            ctrl_mouse.emit(uinput.BTN_LEFT, 0)
            ctrlkey_on = False
