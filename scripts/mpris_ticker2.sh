#!/bin/bash

WIDTH=25
START=0
END=$WIDTH
START_SND=0
END_SND=1

for (( ; ; ))
do	
	FULL_LINE="$(playerctl metadata album) - $(playerctl metadata title) /// "
	if [[ "$PREV_LINE" != "$FULL_LINE" ]]; then
		START=0
		END=$WIDTH
		START_SND=0
		END_SND=1
	fi
	LEN=$(echo "$FULL_LINE" | wc -m)
	LEN=$(($LEN-1))
	if [[ "$LEN"-5 -gt "$WIDTH" ]]; then
		NEW_LINE=$(echo "${FULL_LINE:$START:$END}")
		if [[ "$START" -gt "$LEN"-"$WIDTH" ]]; then
			NEW_LINE=$NEW_LINE$(echo "${FULL_LINE:$START_SND:$END_SND}")
			END_SND=$(($END_SND+1))
			START=$(($START+1))
			if [[ "$END_SND" -eq "$WIDTH" ]]; then
				START=0
				END=$WIDTH
				END_SND=1
			fi
		else
			START=$(($START+1))
		fi
		COL_LEN=$(echo "$NEW_LINE" | wc -L)
		while_START=0
		while_END=$WIDTH
		while [ $COL_LEN -gt $WIDTH ]
		do
			while_END=$(($while_END-1))
			NEW_LINE=${NEW_LINE:$while_START:$while_END}
			COL_LEN=$(echo "$NEW_LINE" | wc -L)
		done
		if [[ $COL_LEN -lt $WIDTH ]]; then
			NEW_LINE="$NEW_LINE " 
		fi
		echo "$NEW_LINE"
	else
		echo "$(playerctl metadata artist) - $(playerctl metadata title)"
	fi
	PREV_LINE="$FULL_LINE"
	sleep 0.5
done
