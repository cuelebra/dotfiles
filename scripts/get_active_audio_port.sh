#!/bin/sh
ACTIVE_PORT=$(pactl list sinks | awk -v FS='Active Port: ' 'NF>1{print $2}')

function get_volume {
    amixer get Master | grep '%' | head -n 1 | cut -d '[' -f 2 | cut -d '%' -f 1
}

function is_mute {
    amixer get Master | grep '%' | grep -oE '[^ ]+$' | grep off > /dev/null
}

if is_mute; then
	printf " 0%%"
else
if [ "$ACTIVE_PORT" = "analog-output-headphones" ]; then
	amixer get Master | grep '%' | head -n 1 | cut -d '[' -f 2 | cut -d '%' -f 1 | awk '{ printf " %s%", $o}'
else
	amixer get Master | grep '%' | head -n 1 | cut -d '[' -f 2 | cut -d '%' -f 1 | awk '{ printf " %s%", $o}'
fi
fi
