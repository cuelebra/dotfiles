#!/bin/sh

CHARGE=$(upower -i $(upower -e | grep 'BAT') | grep "percentage" | tail -c 4)
STATUS=$(upower -i $(upower -e | grep 'BAT') | grep "state" | cut -d " " -f20)

if [[ "$STATUS" = "pending-charge" ]]; then
	MESSAGE=" $CHARGE"
else
	MESSAGE=" $CHARGE"
fi

echo "$MESSAGE"